package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"strings"
	"time"
)

var errBadRequest = errors.New("bad request method")

type playRequest struct {
	PlayURL string   `json:"play_url"`
	CastURL string   `json:"cast_url"`
	MPVArgs []string `json:"mpv_args"`
}

func main() {
	listenAddr := "localhost:7531"
	if v := os.Getenv("LISTEN_ADDRS"); v != "" {
		listenAddr = v
	}

	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()

	mux := http.NewServeMux()
	mux.HandleFunc("/", simpleHandler(appCtx))

	server := http.Server{
		Addr: listenAddr,
		BaseContext: func(net.Listener) context.Context {
			return appCtx
		},
		Handler: mux,
	}

	signals := make(chan os.Signal)
	signal.Notify(signals, os.Kill, os.Interrupt)

	go func() {
		<-signals
		cancelAppCtx()

		ctx, cancelCtx := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelCtx()

		if err := server.Shutdown(ctx); err != nil {
			log.Println("server.Shutdown", err)
		}
	}()

	log.Println("running server on:", listenAddr)
	if err := server.ListenAndServe(); err != nil {
		log.Println("ListenAndServe:", err)
	}
}

func simpleHandler(appCtx context.Context) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		playRequest, err := newPlayRequestFromRequest(r)
		if err != nil {
			status := http.StatusInternalServerError
			if errors.Is(err, errBadRequest) {
				status = http.StatusBadRequest
			}
			http.Error(w, err.Error(), status)
			return
		}

		if playRequest.PlayURL != "" {
			log.Println("playing url:", playRequest.PlayURL)
			cmd := playURL(appCtx, playRequest.PlayURL, playRequest.MPVArgs)
			writeInfo(appCtx, w, cmd, "playing")
			return
		} else if playRequest.CastURL != "" {
			log.Println("playing cast url:", playRequest.CastURL)
			cmd := castURL(appCtx, playRequest.CastURL)
			writeInfo(appCtx, w, cmd, "casting")
			return
		}

		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
	}
}

func playURL(ctx context.Context, uri string, mpvArgs []string) *exec.Cmd {
	var (
		cmdName string
		args    []string
	)

	if isStream(uri) {
		cmdName = "peerflix"
		args = []string{"-k", uri, "--", "--force-window"}
	} else {
		cmdName = "mpv"
		args = []string{uri, "--force-window"}
	}
	args = append(args, mpvArgs...)

	return exec.CommandContext(ctx, cmdName, args...)
}

func castURL(ctx context.Context, uri string) *exec.Cmd {
	var args []string

	if isStream(uri) {
		log.Println("WARNING: Casting torrents not yet fully supported!")
		args = []string{"--video", "--source-url", "http://localhost:8888"}
	} else {
		args = []string{"--video", "-y", uri}
	}

	return exec.CommandContext(ctx, "mkchromecast", args...)
}

func isStream(uri string) bool {
	return strings.HasPrefix(uri, "magnet:") || strings.HasSuffix(uri, ".torrent")
}

func writeInfo(
	ctx context.Context,
	w http.ResponseWriter,
	cmd *exec.Cmd,
	comment string,
) {
	if err := cmd.Start(); err != nil {
		http.Error(w, "error cmd.Start on "+comment+" "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "text/plain")
	w.Write([]byte(comment + "..."))
	cmd.Wait()
}

func newPlayRequestFromRequest(r *http.Request) (*playRequest, error) {
	var pr playRequest

	switch r.Method {
	case http.MethodGet:
		q := r.URL.Query()

		pr.PlayURL = q.Get("play_url")
		pr.CastURL = q.Get("cast_url")

		mpvArgs := q["mpv_args"]
		if mpvArgs == nil {
			mpvArgs = []string{}
		}
		pr.MPVArgs = mpvArgs
	case http.MethodPost:
		d, err := ioutil.ReadAll(r.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading request body: %w", err)
		}
		defer r.Body.Close()

		if err := json.Unmarshal(d, &pr); err != nil {
			return nil, fmt.Errorf("error parsing json data: %w", err)
		}
	default:
		return nil, errBadRequest
	}

	return &pr, nil
}
